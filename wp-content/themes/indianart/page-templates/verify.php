<?php
/**
 * 
 * Template name: Verify Page
 *
 */
get_header();

?>   

<?php  $user_id= $_REQUEST['success1']; 
//update_user_meta( $user_id, "wpduact_status", 'active' ); 
?>
    <section class="content_block_background" id="cbb">
        <h2 class="page-title"><?php the_title(); ?></h2>
            <section id="row-<?php the_ID(); ?>" class="content_block clearfix">
                <div class="row clearfix">
                    <div class="box two-three">
                    
                    	<p>Your email id has been successfully verified. <br>You will receive approval email shortly.</p>
             
                        
                    </div>
                </div>
            </section>
    </section>

 <?php
	if($user_id != ''){
                $user_info = get_userdata($user_id);
                $status_user = get_usermeta( $user_id, "wpduact_status" );
                if($status_user== 'active'){
                    
                     $url = home_url();
                            header('Location: ' . $url); 

                }
                $user_email= $user_info->user_email ;
                $address = get_user_meta($user_id, 'address',true);
                $firstname  = get_user_meta($user_id, 'first_name',true);
                $first_name = explode(" ", $firstname);
                $firstname = $first_name[0];
                $lastname = $first_name[1];
                $phone = get_user_meta($user_id, 'phone',true);
                $country = get_user_meta($user_id, 'country',true);
                $pincode = get_user_meta($user_id, 'pincode',true); 


                $success_urls = home_url()."/activate/?success1=".$user_id;
                $success_url = '<a href="'.$success_urls.'">'.$success_urls.'</a>';   

                $mailMsg = "Hi Admin!!\nAn New User tried to signup in your site.<br>Detail of user :<br>
                First Name : ".$firstname."<br>
                Email : ".$user_email."<br>
                phone : ".$phone."<br>
                Address : ".$address."<br>
                Country : ".$country."<br>
                 <br>Active link.: ".$success_url;
                $subject = "Approval for user";
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-15' . "\r\n";
                $headers .= 'From: Indian Art <info@indianartfurnitures.com>' . "\r\n";


                $admin_email = get_option('admin_email');
                 //$admin_email = 'siddhant@marketingmindz.in' ;
                $result =wp_mail($admin_email, $subject, $mailMsg,$headers);
           }
?>   
<?php 
get_footer();
?>