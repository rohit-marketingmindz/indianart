<?php
/**
 * Template name: Catalogue Page
 */

get_header(); ?>
<section class="content_block_background" id="cbb"> 
    <h2 class="page-title"><?php the_title(); ?></h2>
    <div class="wrap">
    	<div class="boxs">
		<h2><?php _e('Catalogues'); ?></h2>
        <?php 
				$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array( 'post_type' => 'catalogue', 'posts_per_page' => 5,'order'=>'ASC','paged' => $page, );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
				
				?>
                <div class="exclusive_box">
					<?php
                    if ( has_post_thumbnail()):
                        the_post_thumbnail('featured-image');
                    endif;
                    ?>                
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    
                    	<ul>
                            <?php /*?><li><span>INR <?php the_field('price'); ?></span></li><?php */?>
                            <li><a href="<?php the_permalink(); ?>"><?php _e('View More'); ?></a></li>
                        </ul>
                        
				</div>
            <?php 
				  wp_reset_query();endwhile;wp_pagenavi( array( 'query' => $loop ) ); 
			?>
            <div class="clear"> </div>
            
	</div>
    </div>
</section>
<?php get_footer(); ?>