<?php
/**
 * 
 * Template name: Confirm Page
 *
 */
get_header();

?> 
    <section class="content_block_background" id="cbb">
        <h2 class="page-title"><?php the_title(); ?></h2>
            <section id="row-<?php the_ID(); ?>" class="content_block clearfix">
                <div class="row clearfix">
                    <div class="box two-three">
                    	<p>Your Account has been successfully Activated.</p>
                    </div>
                </div>
            </section>
    </section>
<?php 
get_footer();
?>