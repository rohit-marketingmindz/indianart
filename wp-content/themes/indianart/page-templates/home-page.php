<?php
/**
 * Template name: Home Page
 */

get_header(); ?>
		
<?php // echo do_shortcode("[slider id='94' name='Main Slider' size='full']"); ?>
<div class="header-banner">
    <?php echo do_shortcode("[cycloneslider id='main']"); ?>
</div>
<div class="wrap">
	<?php 
	 $id = get_the_ID(); 
	 $text_for_us = get_post_meta($id, "what_we_do_for_us");
	 $text_for_you = get_post_meta($id, "what_we_do_for_you");
     ?>
     <div class="under-header">
        <div class="header_content">
            <?php
        	   echo $text_for_you[0];        
        	?>
        </div>
    </div>
</div>

<?php get_footer(); ?>