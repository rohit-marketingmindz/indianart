<?php
ob_start();
/**
 * 
 * Template name: Login Page
 *
 */
get_header();
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>





if($_POST) {

	if(!empty($_REQUEST['username']) && !empty($_REQUEST['password'])){
		global $wpdb;
		
		//We shall SQL escape all inputs
		$username = $wpdb->escape($_REQUEST['username']);
		$password = $wpdb->escape($_REQUEST['password']);
		$remember = $wpdb->escape($_REQUEST['rememberme']);
		$redirect_to = $_REQUEST['redirect_to'];
		
		if($remember) $remember = "true";
		$remember = "false";
		
		$login_data = array();
		$login_data['user_login'] = $username;
		$login_data['user_password'] = $password;
		$login_data['remember'] = $remember;
		;
		$user_verify = wp_signon( $login_data, false ); 
		
		if ( is_wp_error($user_verify) ) {
			wp_redirect(home_url("/login/?login=failed"));
			exit;
		} else {
			if($remember=="true") $_SESSION['remember'] = true;
			else unset($_SESSION['remember']);
			
			wp_redirect(!empty($redirect_to)?$redirect_to:home_url("/login/?login=true"));
			exit;
		}
	}else{
		wp_redirect(home_url("/login/?login=failed"));
		exit;
	}
}
?>
<section class="content_block_background">
	<section id="row-<?php the_ID(); ?>" class="content_block clearfix">
		<div class="box two">
        <?php
			if ( is_user_logged_in() ) {
		?>
		<div class="user-info">
        	<?php
				$current_user = wp_get_current_user();
				echo 'Logged Email: ' . $current_user->user_email . '<br />';
				echo '<a href="'.wp_logout_url( home_url() ).'" title="Logout">Logout</a>';
				
			?>
            
        </div>
        <?php 
			} ?> 
        <div class="signin-form">
			<form name="loginform" id="loginform" action="<?php echo home_url('/exclusive-collection/'); ?>" method="post" onsubmit="return vali();">
				<p class="login-username">
					<label for="user_login">Username</label> <span class="icon-help-16"></span>
					<input type="text" name="username" id="user_login" class="input" value="" size="20" />
				</p>
				<p class="login-password">
					<label for="user_pass">Password</label> <span class="icon-help-16"></span>
					<input type="password" name="password" id="user_pass" class="input" value="" size="20" />
				</p>
				
				<p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label></p>
				<p class="login-submit">
					<input type="submit" name="wp-submit" id="wp-submiteee" class="button-primary group1" value="Login" />
                    <input type="hidden" name="redirect_to" value="<?php echo isset($_REQUEST['redirect_to'])?$_REQUEST['redirect_to']:''; ?>" />
				</p>
			</form>
    	</div>
		<a href="<?php echo home_url( '/forgot-password/'); ?>">Forgot Password</a> | 
		<a href="<?php echo home_url( '/sign-up/'); ?>">Register</a>       
        
        </div>
        
	</section>
</section>
<?php get_footer(); ?>