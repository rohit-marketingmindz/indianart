<?php
/**
 * 
 * Template name: Recovery Password
 *
 */
 ob_start();
 
global $rt_sidebar_location;
get_header();
global $wpdb;


if($_POST['pass_submit']){
	if(!empty($_POST['new_pass']) && $_POST['new_pass']==$_POST['confirm_pass']){
		
		//$user = check_password_reset_key($_POST['key'], $_POST['login']);
		
		$user = $wpdb->get_row("SELECT * FROM $wpdb->users WHERE user_login='".$_POST['login']."' AND user_activation_key='".$_POST['key']."'");
		
		if(!is_wp_error($user)){
                
			$update_user = wp_update_user( array (
					'ID' => $user->ID, 
					'user_pass' => $_POST['new_pass']
				)
			);
			
			wp_redirect(home_url("/exclusive-collection?success=passrecovered"));
			exit;
			 
		}else{
			$error = __('Somthing went wrong.');
		}
	}else{
		$error = __('Password must be filled.');
	}
}
?>
<section class="content_block_background" id="cbb">
<h2 class="page-title"><?php the_title(); ?></h2>
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>

	<section id="row-<?php the_ID(); ?>" class="content_block clearfix reset-pass">
		
        <div class="signin-form">
		
			<script type="text/javascript">
		
				document.addEventListener("DOMContentLoaded", function() {
				
					// JavaScript form validation
					
					var checkPassword = function(str)
						{
							var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
							return re.test(str);
						};
					
					var checkForm = function(e)
						{
							if(this.currentpassword.value == "") {
								alert("Error: Current Password cannot be blank!");
								this.currentpassword.focus();
								e.preventDefault(); // equivalent to return false
								return;
							}
							/* re = /^\w+$/;
							if(!re.test(this.currentpassword.value)) {
							alert("Error: Current Password cannot be blank!");
							this.currentpassword.focus();
							e.preventDefault();
							return;
							}*/
							if(this.newpassword.value == "" && this.newpassword.value != this.confirmpassword.value) {
								this.newpassword.focus();
								e.preventDefault();
								return;
							}
						};
					
					var myprofile = document.getElementById("resetpassform");
					myprofile.addEventListener("pass_submit", checkForm, true);
					
					// HTML5 form validation
					
					var supports_input_validity = function()
						{
						var i = document.createElement("input");
						return "setCustomValidity" in i;
						}
					
					if(supports_input_validity()) {
						var newpasswordInput = document.getElementById("new_pass");
						newpasswordInput.setCustomValidity(newpasswordInput.title);
						
						var confirmpasswordInput = document.getElementById("confirm_pass");
						
						newpasswordInput.addEventListener("keyup", function() {
							this.setCustomValidity(this.validity.patternMismatch ? newpasswordInput.title : "");
							if(this.checkValidity()) {
								confirmpasswordInput.pattern = this.value;
								confirmpasswordInput.setCustomValidity(confirmpasswordInput.title);
							} else {
								confirmpasswordInput.pattern = this.pattern;
								confirmpasswordInput.setCustomValidity("");
							}
						}, false);
						
						confirmpasswordInput.addEventListener("keyup", function() {
							this.setCustomValidity(this.validity.patternMismatch ? confirmpasswordInput.title : "");
						}, false);
					
					}
				
				}, false);
			
			</script>
		
        	<form method="post" autocomplete="off" name="resetpassform" id="resetpassform">
            	<input type="hidden" name="login" value="<?php echo $_GET['login'] ?>" autocomplete="off">
				<input type="hidden" name="key" value="<?php echo $_GET['key']; ?>" />
				<?php if(!empty($error)){ ?><p><?php echo $error; ?></p><?php } ?>
            	
				<p class="heading-reset"><?php _e('Reset your Password.'); ?></p>
				<p class="mandatory"><?php _e('* All fields are mandatory.'); ?></p>
				
            	<p class="login-password">
                	<label class="forget-label"><?php _e('New Password *'); ?></label>
                    <input type="password" class="input forget" name="new_pass" id="new_pass" value="" title="<?php _e('Password must contain at least 6 characters, including lowercase and numbers.', 'rttheme'); ?>" required pattern="(?=.*\d)(?=.*[a-z]).{6,}" />
                </p>
                <p class="login-password">
                	<label class="forget-label"><?php _e('Confirm Password *'); ?></label>
                    <input type="password" class="input forget" name="confirm_pass" id="confirm_pass" value="" title="<?php _e('Please enter the same Password as above.', 'rttheme'); ?>" required pattern="(?=.*\d)(?=.*[a-z]).{6,}" />
                </p>
                <p class="login-submit">
                <input type="submit" name="pass_submit" class="button-primary forget-submit" id="pass_submit" value="Reset Password" />
                </p>
            </form>
        </div>
        
    </section>
</section>
<?php get_footer(); ?>