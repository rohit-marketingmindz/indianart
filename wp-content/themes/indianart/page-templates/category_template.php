<?php
/**
 * Template name: Category Page
 */
ob_start();
get_header(); ?>
<section class="content_block_background" id="cbb">
<h2 class="page-title"><?php the_title(); ?></h2>

<div class="wrap">
	<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
        <div class="boxs">
        	<h2><?php the_title(); ?></h2>
        <?php 
		
			 $catargs = array(
				'orderby' => 'name',
				'order'=> 'ASC',
				
				); 
				
					$categories = get_categories( $catargs );
					foreach ($categories as $category) {
						//print_r($categories);
						//echo $category->cat_ID;
					$args = array (
						'exclude'=>'13',
						'post_type'=> 'products',
						'cat' => $category->cat_ID,
						'posts_per_page'=> '-1',
						'order' => 'ASC',
						'orderby'=> 'title',
						
			);
			// The Query
			$query = new WP_Query( $args );
			// The Loop
			if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
			$query->the_post();
        ?>
        <div class="exclusive_box">
			<?php
				if ( has_post_thumbnail()):
				the_post_thumbnail( 'featured-image');
				endif;
            ?>                
        	<h3><?php the_title(); ?></h3>
        
            <ul>
            <?php /*?><li><span>INR <?php the_field('price'); ?></span></li><?php */?>
            <li><a href="<?php the_permalink(); ?>"><?php _e('View More'); ?></a></li>
            </ul>
        </div>
        <?php 
			}
			} 
			wp_reset_postdata();
			}
        ?>
        <div class="clear"> </div>
        
        </div>
</div>
         
      </section> 
        
<?php get_footer(); ?>