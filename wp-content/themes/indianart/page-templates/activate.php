<?php
/**
 * 
 * Template name: Activate Page
 *
 */
get_header();

?>   

    <section class="content_block_background" id="cbb">
        <h2 class="page-title"><?php the_title(); ?></h2>
            <section id="row-<?php the_ID(); ?>" class="content_block clearfix">
                <div class="row clearfix">
                <?php
                $user_id= $_REQUEST['success1'];
                $status_user = get_usermeta( $user_id, "wpduact_status" );
                if($status_user== 'active'){
                    
                     $url = home_url();
                            header('Location: ' . $url); 

                }
                     if (isset($_POST['submit']) && $_POST['submit'] == 'approved') {
                             if(!empty($_POST['crm'] ))
                                {
                                   $user_info = get_userdata($user_id);
                            $user_email = $user_info->user_email ;
                            $address = get_user_meta($user_id, 'address',true);
                            $firstname  = get_user_meta($user_id, 'first_name',true);
                            $first_name = explode(" ", $firstname);
                            $firstname = $first_name[0];
                            $lastname = $first_name[1];
                            $phone = get_user_meta($user_id, 'phone',true);
                            $country = get_user_meta($user_id, 'country',true);
                            $pincode = get_user_meta($user_id, 'pincode',true);  
                                // User insert in CRM
                                    lead_insert_syncing($user_email,$address,$firstname,$lastname,$phone,$country,$pincode);
                                    $url = home_url()."/success/";
                                    header('Location: ' . $url);
                                }else{
                                    $url = home_url()."/success/";
                                    header('Location: ' . $url);

                                }
                            }
                     $user_id= $_REQUEST['success1'];?>
                    <?php if (isset($_POST['submit']) && $_POST['submit'] == 'approve') {
                          
                            if(!empty($_POST['yes'] ))
                            { 

                            $user_info = get_userdata($user_id);
                            $user_email= $user_info->user_email ;
                            $address = get_user_meta($user_id, 'address',true);
                            $firstname  = get_user_meta($user_id, 'first_name',true);
                            $first_name = explode(" ", $firstname);
                            $firstname = $first_name[0];
                            $lastname = $first_name[1];
                            $phone = get_user_meta($user_id, 'phone',true);
                            $country = get_user_meta($user_id, 'country',true);
                            $pincode = get_user_meta($user_id, 'pincode',true); 
                        
                            ?>

                                <div class="approve_user">
                                <form method="post" action="" id="user-approve">
                                    <span>Add to CRM Lead</span>
                                    <input type="radio" name="crm" value="1" checked="checked"><b>YES</b>
                                    <input type="radio" name="crm" value="0"><b>NO</b>
                                    <input type="submit" name="submit" value="approved"/>
                                </form>
                            </div>
                        <?php 
                           
                            $uri = get_template_directory_uri().'/images/img.png';
                            $uri1 = get_template_directory_uri().'/images/vriksh.png';
                            $uri2 = get_template_directory_uri().'/images/epch-1.png';
                            $uri3 = get_template_directory_uri().'/images/iso.png';
                            $uri4 = get_template_directory_uri().'/images/FSC.png';
                            $img_url = '<img src="'.$uri.'">';

                            $logo_url = '<img src="'.$uri1.'" width="80px" style="margin-left:10px;">';
                            $logo_url1 = '<img src="'.$uri2.'" width="80px" style="margin-left:10px;">';
                            $logo_url2 = '<img src="'.$uri3.'" width="80px" style="margin-left:10px;">';
                            $logo_url3 = '<img src="'.$uri4.'" width="80px" style="margin-left:10px;">';
                            $excul_url = site_url().'/exclusive-collection';
                            $cert_url = site_url().'/certifications/';
                            $mailMsg = "Hi Admin!!\nAn User Approved in your website indianartfurnitures.com.";
                            $subject = "User Approved";
                            $headers = 'From: Indian Art <info@indianartfurnitures.com>' . "\r\n";
                            $admin_email = get_option('admin_email');
                            $result =wp_mail($admin_email, $subject, $mailMsg,$headers);

                            $mailMsg = "<div style='line-height:20px;'><b>Hello ". ucfirst($first_name[0]) .",</b><br/><br/><b>Thanks for your interest, now you can visit 'Exclusive Collection' of<br> IndianArt Furnitures Pvt. Ltd. Please use similar credentials to login <br>to  our aura of  <a href=" . $excul_url . "><b>Exclusive collections</b></a> which you have created while registration process.  <br><br><a href=" . $cert_url . ">". $logo_url."</a> <a href=" . $cert_url . ">" . $logo_url1. "</a> <a href=" . $cert_url . ">" .$logo_url2."</a> <a href=" . $cert_url . ">" .$logo_url3."</a><br><br>We always feel delighted to serve our happy customers.<br><br<br><br>". $img_url ."<br><b>Indian Art Furnitures Pvt. Ltd.</b><br>
            Ph. :: +91 141 6510124 Fax: +91 141 277 1724<br><br>

            <b style='text-align:justify;'>Disclaimer: ******************************************************</b><br>
            This email (including any attachments) is intended for the sole use of<br>
            the intended recipient/s and may contain material that is CONFIDENTIAL AND<br>
            PRIVATE COMPANY INFORMATION. Any review or reliance by others or copying<br> or
            distribution or forwarding of any or all of the contents in this message is<br>
            STRICTLY PROHIBITED. If you are not the intended recipient, please contact<br>
            the sender by email and delete all copies; your cooperation in this regard<br>
            is appreciated.<B>
            <br><b>******************************************************************</b></div>";
                            $subject = "User Approved";
                            $headers = 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: text/html; charset=iso-8859-15' . "\r\n";
                            $headers .= 'From: Indian Art <info@indianartfurnitures.com>' . "\r\n";
                            $admin_email = get_option('admin_email');
                            $result = wp_mail($user_email, $subject, $mailMsg,$headers);
                            update_user_meta( $user_id, "wpduact_status", 'active' ); 
                           

                        } else {
                            $url = home_url();
                            header('Location: ' . $url); 
                          
                        }
                    }

                    ?>
                    <?php if (isset($_POST['submit']) && $_POST['submit'] == 'approve') {
                        ?>
                        <style type="text/css">
                        .user_form{display: none;}
                        </style>
                     <?php   } 
                        ?>
                    <div class="approve_user user_form">
                        <form method="post" action="" id="user-approve">
                            <span>PLEASE APPROVE THE USER</span>
                            <input type="radio" name="yes" value="1" checked="checked"><b>YES</b>
                            <input type="radio" name="yes" value="0"><b>NO</b>
                            <input type="submit" name="submit" value="approve"/>
                        </form>
                    </div>
                    
                </div>
            </section>
    </section>
<?php 
get_footer();

function lead_insert_syncing($user_email,$address,$firstname,$lastname,$phone,$country,$pincode)
{ 
    $firstname          = $firstname;
    $lastname           = ($lastname == "" ? "????" : $lastname);
    $primaryphone       = '';
    $company            = '';
    $mobile             = $phone;
    $designation        = '';
    $primaryemail       = $user_email;
    $secondaryemail     = '';
    $website            = "www.indianartfurnitures.com";
    $employee           = '';
    $annualrevenue      = '';   
    $street             = $address;
    $fax                = '';
    $postalcode         = $pincode;
    $city               = '';
    $pobox              = '';
    $state              = '';
    $country            = $country;
    $date               = date('Y-m-d H:i:s');
    $label              = $firstname." ".$lastname;

        $mydb = new wpdb('inafpl_erpin','erpin123!#','inafpl_crm','localhost'); 

        $crmid_res = $mydb->get_results("SELECT * from vtiger_crmentity_seq");
        
        foreach ($crmid_res as $obj2) :
            $crmid = $obj2->id;
        endforeach;
 
        $crmid =$crmid+1;
        
        $lead_seq_res = $mydb->get_results("SELECT * from vtiger_modentity_num where semodule='Leads'");
        
        foreach ($lead_seq_res as $obj3) :
            $lead_seq =  $obj3->cur_id;
            $lead_prefix =  $obj3->prefix;
        endforeach;
        
        $lead_seq = $lead_seq;
        
        $vtiger_leaddetails =  "INSERT INTO `vtiger_leaddetails` SET 
        `leadid` = '" .$crmid . "',
        `lead_no` = '$lead_prefix".$lead_seq."',
        `firstname` = '" . $firstname . "',
        `lastname` = '" .$lastname . "',
        `company` = '" .$company . "',
        `annualrevenue` = '" .$annualrevenue . "',
        `email` = '" .$primaryemail . "',
        `designation` = '" .$designation . "'";
        
        $vtiger_leaddetailsa = $mydb->query($vtiger_leaddetails);
        $lead_seq = $lead_seq+1;
        $update_lead_seq = "update vtiger_modentity_num SET cur_id = '$lead_seq' where semodule='Leads'";
        $mydb->query($update_lead_seq);
        
    
        $vtiger_leadaddress = "INSERT INTO `vtiger_leadaddress` SET 
        `leadaddressid` = '" .$crmid . "',
        `city` = '" .$city . "',
        `code` = '" .$postalcode . "',
        `state` = '" .$state . "',
        `pobox` = '" .$pobox . "',
        `country` = '" .$country . "',
        `phone` = '" .$primaryphone . "',
        `mobile` = '" .$mobile . "',
        `fax` = '" .$fax . "',
        `lane` = '" .$street . "'";
        
        $mydb->query($vtiger_leadaddress);
        

        
        $leadscf = "INSERT INTO `vtiger_leadscf` SET 
        `leadid` = '" .$crmid . "'";
        
        $mydb->query($leadscf);
        
        $vtiger_leadsubdetails = "INSERT INTO `vtiger_leadsubdetails` SET
        `website` = '" .$website . "',
        `leadsubscriptionid` = '" .$crmid . "'";
        
        $mydb->query($vtiger_leadsubdetails);
        
        
        $vtiger_crmentity = "INSERT INTO `vtiger_crmentity` SET 
        `crmid` = '" .$crmid . "',
        `smcreatorid` = 1,
        `smownerid` = 1,
        `modifiedby` = 1,
        `setype` = 'Leads',
        `description` = '',
        `createdtime` = NOW(),
        `modifiedtime` = NOW(),
        `label` = '$label',
        `presence` = 1";
        
        $mydb->query($vtiger_crmentity);
        
        $update_crmentity_seq = "update vtiger_crmentity_seq SET id = '$crmid'";
        
        $mydb->query($update_crmentity_seq);
    }
?>