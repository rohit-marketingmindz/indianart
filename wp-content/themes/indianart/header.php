<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
session_start();
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="8VCyp0VsvAoaQ2Ta9UcxTIKFlsOjH7FdHNsz2MDuF08" />
	<!-- <meta name="viewport" content="width=device-width"> -->
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php //echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico">
    <link href="http://fonts.googleapis.com/css?family=Carrois+Gothic" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
   <!--  <link href="css/fonts.css" rel="stylesheet" type="text/css" /> -->
		<style type="text/css">
        .body
        {
        font-family: 'Carrois Gothic SC' , sans-serif !important;
        }
		.clear{
			clear:both;
		}
        </style>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/banner.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/fonts.css">

	<?php wp_head(); ?>
	<script>
		jQuery(document).ready(function(){
			jQuery(".mm-mobile_menu").click(function(){
				jQuery(".mm-header_menus ul").toggle('slow');
			});
            jQuery('#product-loginform #user_login').attr('placeholder', 'Username');
            jQuery('#product-loginform #user_pass').attr('placeholder', 'Password');
            jQuery("#product-loginform a:contains('Forgot Password?')").show();
            
		});
		if(jQuery(window).width() > 1023){
			/* sticky menu js */
				function sticky_relocate() {
					var window_top = jQuery(window).scrollTop();
					var div_top = jQuery('#sticky-anchor').offset().top;
					if (window_top > div_top) {
						jQuery('#sticky').addClass('stick');
                        <?php if(is_user_logged_in()){ ?>
                            jQuery('#sticky').addClass('current');
                        <?php } ?>
						jQuery('#sticky-anchor').height(jQuery('#sticky').outerHeight());
						jQuery('#top-header-left').css("display", "none");
		
					} else {
						jQuery('#sticky').removeClass('stick');
                        <?php if(is_user_logged_in()){ ?>
                            jQuery('#sticky').removeClass('current');
                        <?php } ?>
						jQuery('#sticky-anchor').height(0);
						jQuery('#top-header-left').css("display", "block");
					}
				}
				jQuery(function() {
				jQuery(window).scroll(sticky_relocate);
				sticky_relocate();
				});		
			/* sticky menu js end */
		}
	</script>
<!-- Css by Pradeep TR Shamra -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/mm_indianart.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/mm_responsive.css"> 
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/mm_responsive.css"> 
<!-- Css by Pradeep TR Shamra -->
</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84144271-1', 'auto');
  ga('send', 'pageview');

</script>


<body <?php body_class(); ?>>
<div class="container" id="mm-container">
    <div id="sticky-anchor"></div>
    <div id="sticky" class="top">
        <div class="mm-header">
            <div class="mm-logo">
                <a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_option('wpc_logo'); ?>" border="0" />
                </a>
            </div>
            <div class="mm-header_right">
                <div class="mm-login-sec" id="top-header-left">
                    <div class="mm-log-part">
                        <div id="mm-login-bth">
                            <?php do_action("add_member_box"); ?>
                        </div>                   
                        <div class="mm-signup-btn">
                            <?php if ( !is_user_logged_in()) { ?>
                                <a href="<?php echo get_page_link( get_page_by_title('Sign Up')) ?>">
                                <button class="signupp res-sign"><?php _e('Sign Up'); ?>
                                    
                                </button>
                                </a>
                            <?php }  ?>
                        </div>
                        <div class="mm-header-inquire">
                             <?php $site = site_url()."/";
                             $current_url =  'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
                             if($site != $current_url && $_SESSION['value']!= '')
                                {?>
                                    <div class="mm-header-cart but_cart">
                                        <span class="cart-icon">
                                        
                                                <img src="<?php echo get_template_directory_uri();?>/images/cart.png" alt="">
                                        </span>
                                        <a  href="<?php echo site_url('contact-us'); ?>">
                                            <?php _e('Enquire'); ?>
                                            <span id="show_value" class="noti_number"><?php echo $_SESSION['value']; ?>
                                            </span>
                                        </a>
                                    </div>
                            <?php } ?>
                        </div>
                        <div class="mm-language">
                            <?php do_action('icl_language_selector'); ?>
                        </div>    
                    </div>
                </div>
            
                <div class="mm-header_menus">
                	<div class="mm-mobile_menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <?php $defaults = array( 'theme_location' => '', 'menu'=> 'header_menu','container'=>'header_menus'); wp_nav_menu( $defaults ); ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
