<?php

/**
 * Products Posttype
 *
 */
add_action('init', 'create_products');

function create_products() {

    $labels = array(
        'name' => __('Products', 'post type general name'),
        'singular_name' => __('Products', 'post type singular name'),
        'add_new' => __('Add Products', 'portfolio item'),
        'add_new_item' => __('Add Product'),
        'edit_item' => __('Edit Product'),
        'new_item' => __('New Product'),
        'view_item' => __('View Product'),
        'search_items' => __('Search Products'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail'),
		'taxonomies'=>array('category')
      ); 

    register_post_type( 'products' , $args );
}
////// Range Taxanomy/////////
add_action( 'init', 'create_range_tax' );

function create_range_tax() {
	register_taxonomy(
		'range',
		'products',
	array(
		'label' => __( 'Range' ),
		'rewrite' => array( 'slug' => 'range' ),
		'hierarchical' => true,
	)
	);

	register_taxonomy(
		'excollection',
		'products',
	array(
		'label' => __( 'Exclusive Collection' ),
		'rewrite' => array( 'slug' => 'excollection' ),
		'hierarchical' => true,
	)
	);
}

/**
 * Products Posttype
 *
 */
 
 /**
 * Catalogue Posttype
 *
 */
add_action('init', 'create_catalogue');

function create_catalogue() {

    $labels = array(
        'name' => __('Catalogue', 'post type general name'),
        'singular_name' => __('Catalogue', 'post type singular name'),
        'add_new' => __('Add Catalogue', 'portfolio item'),
        'add_new_item' => __('Add Catalogue'),
        'edit_item' => __('Edit Catalogue'),
        'new_item' => __('New Catalogue'),
        'view_item' => __('View Catalogue'),
        'search_items' => __('Search Catalogue'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail'),
		'taxonomies'=>array('category')
      ); 

    register_post_type( 'catalogue' , $args );
}


/**
 * Catalogue Posttype
 *
 */
 
 
 /**
 * Video Posttype
 *
 */
add_action('init', 'create_video');

function create_video() {

    $labels = array(
        'name' => __('Video', 'post type general name'),
        'singular_name' => __('Video', 'post type singular name'),
        'add_new' => __('Add Video', 'portfolio item'),
        'add_new_item' => __('Add Video'),
        'edit_item' => __('Edit Video'),
        'new_item' => __('New Video'),
        'view_item' => __('View Video'),
        'search_items' => __('Search Video'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title')
      ); 
		register_post_type( 'video' , $args );
}


/**
 * Video Posttype
 *
 */
 
/**
* add login button in top header
**/
add_action("add_member_box", "callback_add_member_box");
function callback_add_member_box(){
	global $wpdb, $current_user;
		?>
		<script>
		jQuery(document).ready(function(){

			jQuery("#log").click(function(){
				jQuery("#tog-login").toggle('slow');
			});

	     	jQuery('#tog-login').hide()
		});

		 jQuery('#log').on('click', function () {
		     jQuery('#tog-login').show(500)
		 });

		 jQuery(document).click(function (e) {
		     var popup = jQuery("#tog-login");
		     if (!jQuery('#log').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
		         popup.hide(500);
	     	}

		});
		</script>
		
		<div id="log" class="member-box">
		<?php 
			if(!is_user_logged_in())
			{ 
			?>
            <?php _e('Login'); ?>
 			<?php 
			}
			else
			{ 
				//echo "Hello, ".$current_user->first_name." ". $current_user->last_name."";
				echo "Hello, ".$current_user->first_name.""; 
			} 
		?>
        </div>
		<div class="login pos_top" id="tog-login" style="display:none;">
			<?php
				$referrer = $_SERVER['HTTP_REFERER'];
				if($referrer != site_url('/exclusive-collection/')){  
					if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'failed') ){
						echo '<p class="log_invalid">Invalid username or password.</p>';
						echo "<script>jQuery(document).ready(function(){jQuery('.member-box').trigger('click');});</script>";
					}
					
				}
			     ?>
			<?php
			if(!is_user_logged_in()){
				$args = array(
					'echo'           => true,
					'redirect' => 	home_url( '/exclusive-collection/' ),
					'form_id'        => 'loginform',
					'label_username' => __( 'Username' ),
					'label_password' => __( 'Password' ),
					'label_log_in'   => __( 'Log In' ),
					'id_username'    => 'user_login',
					'id_password'    => 'user_pass',
					'id_submit'      => 'wp-submit',
					'value_username' => ''
				); 
			wp_login_form( $args );
			}else{
				$name = trim(get_user_meta($current_user->ID, "first_name", true)." ". get_user_meta($current_user->ID, "last_name", true));
				if(!empty($name)){
					?>
					Name: <?php _e($name); ?><br />
					<?php
				}
				?>
				Email: <?php _e($current_user->user_email); ?><br />
				<a href="<?php echo wp_logout_url( home_url() ); ?>"><?php _e("Logout"); ?></a>
				<?php
			}
		?>
    </div>
    <?php
}

/////////////////////////add forgot password

add_action( 'login_form_middle', 'add_lost_password_link' );
function add_lost_password_link() {
	return '<a href="'.home_url().'/forgot-password">Forgot Password?</a>';
}


#modify recovery password url
function reset_password_message( $message, $key ) {
	
	if ( strpos($_POST['user_login'], '@') ) {
		$user_data = get_user_by('email', trim($_POST['user_login']));
	} else {
		$login = trim($_POST['user_login']);
		$user_data = get_user_by('login', $login);
	}
	
	$user_login = $user_data->user_login;
	
	$msg = __('The password for the following account has been requested to be reset:', 'rttheme'). "\r\n\r\n";
	$msg .= network_site_url() . "\r\n\r\n";
	$msg .= sprintf(__('Username: %s', 'rttheme'), $user_login) . "\r\n\r\n";
	$msg .= __('If this message was sent in error, please ignore this email.', 'rttheme') . "\r\n\r\n";
	$msg .= __('To reset your password, visit the following address:', 'rttheme');
	$msg .= '<' . network_site_url("recovery/?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";
	
	return $msg;

}
add_filter('retrieve_password_message', 'reset_password_message', null, 2);

/**
 * Function Name: front_end_login_fail.
 * Description: This redirects the failed login to the custom login page instead of default login page with a modified url
**/
add_action( 'wp_login_failed', 'front_end_login_fail', 10);
function front_end_login_fail( $username ) {

	// Getting URL of the login page
	$referrer = $_SERVER['HTTP_REFERER'];


	if( !empty( $referrer ) && !strstr( $referrer,'wp-login' ) && !strstr( $referrer,'wp-admin' ) ) {

		if($referrer == site_url('/products/') || $referrer == site_url('/products/?p_login=failed')){ 
			wp_redirect( home_url( '/products/?p_login=failed' ) ); 
		} else {
			wp_redirect( home_url( '/?login=failed' ) ); 
			//wp_redirect( $_SERVER['HTTP_REFERER'] . "?login=failed" );
		}	
	}
}

/** 
 * Function Name: check_username_password.
 * Description: This redirects to the custom login page if user name or password is empty with a modified url
**/

add_action( 'authenticate', 'check_username_password', 1, 3);
function check_username_password( $login, $username, $password ) {
	
	// Getting URL of the login page
	$referrer = $_SERVER['HTTP_REFERER'];
	// if there's a valid referrer, and it's not the default log-in screen
		if($referrer == site_url('/exclusive-collection/')){ 

			if( $username == "" || $password == "" ){
				wp_redirect( home_url('/exclusive-collection/?login=blank') );
				exit;
			}
		}
		if( !empty( $referrer ) && !strstr( $referrer,'wp-login' ) && !strstr( $referrer,'wp-admin' ) ) { 
			if( $username == "" || $password == "" ){


				if($referrer == site_url('/products/') || $referrer == site_url('/products/?p_login=failed')){ 
					wp_redirect( home_url( '/products/?p_login=failed' ) ); 
				} else {
					wp_redirect( home_url( '/?login=failed' ) ); 
					
				}
				exit;
					
			}
		}
}






//////////////////Add image size///////////////////////
add_action( 'after_setup_theme', 'setup' );
	function setup() {
    add_theme_support( 'post-thumbnails' ); // This feature enables post-thumbnail support for a theme
    add_image_size( 'featured-image', 300, 200, false );
	add_image_size( 'range-image', 300, 200, false );
	add_image_size( 'featured-big-image', 360, 240, false );
	add_image_size( 'catalogue-featured-image', 360, 240, false );
	add_image_size( 'single-featured-image', 510, 340, false );
}

///////////////////////////filter products////////////////////

function get_terms_dropdown_type($taxonomies, $args,$cat ){
        $myterms = get_terms($taxonomies, $args);
        $output ="<select name='products' class='select_box'>";
        $output .="<option value=''>All</option>";
        foreach($myterms as $term){
                $root_url = get_bloginfo('url');
                $term_taxonomy=$term->taxonomy;
                $term_slug=$term->slug;
                $term_name =$term->name;
                $link = $term_slug;
                $output .="<option value='".$link;
				if($link == $cat)
				{
					$output .="' selected='selected'>".$term_name."</option>";					
				}
				else
				{
					$output .="'>".$term_name."</option>";
				}
        }
        $output .="</select>";
		return $output;
}


function get_terms_dropdown_typesssss($taxonomies, $args,$cat1 ){
        $myterms = get_terms($taxonomies, $args);
        $output ="<select name='products' class='select_box'>";
        $output .="<option value=''>All</option>";
        foreach($myterms as $term){
                $root_url = get_bloginfo('url');
                $term_taxonomy=$term->taxonomy;
                $term_slug=$term->slug;
                $term_name =$term->name;
                $link = $term_slug;
                $output .="<option value='".$link;
				if($link == $cat1)
				{
					$output .="' selected='selected'>".$term_name."</option>";					
				}
				else
				{
					$output .="'>".$term_name."</option>";
				}
        }
        $output .="</select>";
		return $output;
}
//////////////////////////////
// Numbered Pagination
if ( !function_exists( 'wpex_pagination' ) ) {
	
	function wpex_pagination() {
		
		$prev_arrow = is_rtl() ? '&rarr;' : '&larr;';
		$next_arrow = is_rtl() ? '&larr;' : '&rarr;';
		
		global $wp_query;
		$total = $wp_query->max_num_pages;
		$big = 999999999; // need an unlikely integer
		if( $total > 1 )  {
			 if( !$current_page = get_query_var('paged') )
				 $current_page = 1;
			 if( get_option('permalink_structure') ) {
				 $format = 'page/%#%/';
			 } else {
				 $format = '&paged=%#%';
			 }
			echo paginate_links(array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'		=> $format,
				'current'		=> max( 1, get_query_var('paged') ),
				'total' 		=> $total,
				'mid_size'		=> 3,
				'type' 			=> 'list',
				'prev_text'		=> $prev_arrow,
				'next_text'		=> $next_arrow,
			 ) );
		}
	}
	
}

//////////////////////////user approval///////////////////////

//hooks
add_action( 'show_user_profile', 'Add_user_fields' );
add_action( 'edit_user_profile', 'Add_user_fields' );

function Add_user_fields( $user ) { ?>

<h3>User Approval</h3>
<table class="form-table">
    <tr>
        <th><label for="dropdown">User's Approval</label></th>
        <td>
            <?php 
            //get dropdown saved value
            $selected = get_the_author_meta( 'user_select', $user->ID ); //there was an extra ) here that was not needed 
            ?>
            <select name="user_select" id="user_select">
            	<option value="" <?php echo ($selected == "")?  'selected="selected"' : '' ?>>Select</option>
                <option value="deny" <?php echo ($selected == "deny")?  'selected="selected"' : '' ?>>Deny</option>
                <option value="approve" <?php echo ($selected == "approve")?  'selected="selected"' : '' ?>>Approve</option>
</select>


            <span class="description">Please select any option</span>
        </td>
    </tr>
</table>

<?php }
add_action( 'personal_options_update', 'save_user_fields' );
add_action( 'edit_user_profile_update', 'save_user_fields' );

function save_user_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) )
    return false;

//save dropdown
update_usermeta( $user_id, 'user_select', $_POST['user_select'] );
$user_info = get_userdata($user_id);
if($_POST['user_select'] == "approve"){

$to = $user_info->user_login;
$subject = 'IndianArt Approval Email';
$message = 'You are approved by IndianArt Authority, Now you can login with your login details. Thanks';
wp_mail( $to, $subject, $message );
}
if($_POST['user_select'] == "deny"){

$to = $user_info->user_login;
$subject = 'IndianArt Denied Email';
$message = 'You are denied by IndianArt Authority, Now you cannot login with your login details. Thanks';
wp_mail( $to, $subject, $message );
}
}



/////////////////IP Location///////////////////////
/*Get user ip address*/
$ip_address=$_SERVER['REMOTE_ADDR'];

/*Get user ip address details with geoplugin.net*/
$geopluginURL='http://www.geoplugin.com/php.gp?ip='.$ip_address;
//$addrDetailsArr = unserialize(file_get_contents($geopluginURL));

$curl_handle=curl_init();
curl_setopt($curl_handle, CURLOPT_URL,$geopluginURL);
curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
$query = curl_exec($curl_handle);
$addrDetailsArr = unserialize($query);
echo $addrDetailsArr;

/*Get City name by return array*/
$city = $addrDetailsArr['geoplugin_city']; 

/*Get Country name by return array*/
$country = $addrDetailsArr['geoplugin_countryName'];

/*Comment out these line to see all the posible details*/
/*echo '<pre>';
print_r($addrDetailsArr);
die();*/

if(!$city){
   $city='Not Define';
}
if(!$country){
   $country='Not Define';
}
curl_close($curl_handle);
?>