<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
<!----------------------------------------------------------------------->
    
    
    <?php  $prodtype = array(
		   'show_option_all'    => '',
		   'show_option_none'   => '',
		   'orderby'            => 'ID', 
		   'order'              => 'ASC',
		   'show_count'         => 0,
		   'hide_empty'         => 1, 
		   'child_of'           => 0,
		   'exclude'            => '',
		   'echo'               => 1,
		   'selected'           => 0,
		   'hierarchical'       => 0, 
		   'name'               => 'cat',
		   'id'                 => '',
		   'class'              => 'postform',
		   'depth'              => 0,
		   'tab_index'          => 0,
		   'taxonomy'           => 'range',
		   'hide_if_empty'      => false,
			);
	?>
    
    <form>
<b><?php _e('Product Type'); ?></b><br/>
<?php wp_dropdown_categories($prodtype); ?>
<br/>
<script type="text/javascript"><!--
var dropdown = document.getElementById("cat");
function onRangeChange() {
    if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
        location.href = "<?php echo get_option('home');
?>products/?range="+dropdown.options[dropdown.selectedIndex].value;
    }
}
dropdown.onchange = onRangeChange;
--></script>
<h2>Filter by</h2>

    <form action="<?php bloginfo('url'); ?>" method="get">
    <div>
    <?php
	
	 if(isset($_POST['submit'])){
	  print_r($_REQUEST);
	  die;
  }
    $taxonomies = array('range');
    $args = array('orderby'=>'name','hide_empty'=>false);
    $select = get_terms_dropdown_grade_level($taxonomies, $args);
    $select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
    echo $select;
    ?>
    
    <input type="submit" name="submit" value="filter" />
    </div>
    </form>
    <!------------------------------------------------------------------------->
		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'twentythirteen' ), get_the_date() );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'twentythirteen' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentythirteen' ) ) );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'twentythirteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentythirteen' ) ) );
					else :
						_e( 'Archives', 'twentythirteen' );
					endif;
				?></h1>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
