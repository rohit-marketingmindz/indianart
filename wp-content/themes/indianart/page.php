<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<?php global $post;
	$currentpageid=$post->ID;
 ?>
	<section class="content_block_background" id="cbb">
			<h2 class="page-title"><?php the_title(); ?></h2>
			<?php /* The loop */ ?>
			<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>
			<?php endwhile; ?>
			<div class="linking-menu">
			<ul>
			<li><?php echo "Related Links:"; ?></li>

			<?php 

				$args = array(	
				'include' => array(11, 19, 687, 717),
				'post_type' => 'page',
				); 
				//echo '<span>'. $page->post_title. '</span>'; 
				$pages = get_pages($args);

				foreach($pages as $page){
					/*echo "<pre>";
					print_r($page);*/
					if($currentpageid == $page->ID) continue; ?>

					<li><a href="<?php echo esc_url( get_permalink($page->ID) ); ?>"><?php echo $page->post_title;?></a></li>
					
				<?php }?>
				</ul></div>

		</section>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>