    <?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<?php
$path_product1 = $_SERVER['REQUEST_URI'];
$path_arr1 = explode("?",$path_product);
$path_product1 = $path_arr1[0];
$cat1 = "";
$separator = " » ";
if(isset($_REQUEST['cat1234']))
  {
      $cat1 = trim($_REQUEST['cat1234']);
  }
?>
    <section class="content_block_background" id="cbb">
   
   
        <?php  if( is_tax() ) {
    global $wp_query;
    $term = $wp_query->get_queried_object(); ?>
        <h2 class="page-title"><?php echo $title = $term->name; }?></h2>
        
        <?php 
        echo '<div class="breadcrumb">';
        echo '<a href="';
        echo get_option('home');
        echo '">';
        bloginfo('name');
        echo "</a> ".$separator;
        echo '<a href="';
        echo get_option('home').'/exclusive-collection';
        echo '">';
        echo "Excollection";
        echo "</a> ".$separator;
        echo $title;
        echo '</div>';
        ?> 


<?php
            if( !is_user_logged_in() ){
                if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'failed') ){
                    echo '<p style="color:red;">Invalid username or password</p>';
                }
                if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'blank') ){
                    echo '<p style="color:red;">Please fillout your username or password.</p>';
                }
                if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'blank?login=failed') ){
                    echo '<p style="color:red;">Invalid username or password</p>';
                }
                if(!empty($_REQUEST['register']) &&($_REQUEST['register'] == 'true')){
                    echo '<p style="color:green;">You are successfully registered. Check your email and activate your account.</p>';
                }
                if(!empty($_REQUEST['success']) &&($_REQUEST['success'] == 'passrecovered')){
                    echo '<p style="color:green;">Your password has been successfully updated.</p>';
                }
                if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'required') ){
                    echo '<p style="color:red;">You have to login first.</p>';
                }
                if(!empty($_REQUEST['reset']) &&($_REQUEST['reset'] == 'true') ){
                    echo '<p style="color:green;">Please check your email address, we send a link to change password.</p>';
                }
                if(!empty($_REQUEST['activation']) &&($_REQUEST['activation'] == 'true') ){
                    echo '<p style="color:green;">Your Account has been successfully Activated. Admin approval required before login. We will send you an email after approval.</p>';
                    $nwuserID = $_REQUEST['uid'];
                    update_user_meta($nwuserID,'user_select');
                    
                }
        ?>

<div class="exclu_bfr_login">
    <div class="exclu_login">
        <h2><?php _e('Registered Users'); ?></h2>
        <hr/>
        <?php
                $args = array(
                    'echo'           => true,
                    'redirect' =>   home_url( '/exclusive-collection' ),
                    'form_id'        => 'loginform',
                    'label_username' => __( 'Username' ),
                    'label_password' => __( 'Password' ),
                    'label_log_in'   => __( 'Log In' ),
                    'id_username'    => 'user_login',
                    'id_password'    => 'user_pass',
                    'remember' => true,
                    'id_submit'      => 'wp-submit',
                    'value_username' => ''
                ); 
            wp_login_form( $args );
            ?>
    </div>
    <div class="exclu_signup">
        <h2><?php _e('New User'); ?></h2>
        <hr/>
        <p><?php _e('By creating an account you will be able to see our exclusive collection...')?></p>
        <a href="<?php echo get_page_link( get_page_by_title('Sign Up')) ?>"><button class="signupp"><?php _e('Sign Up'); ?></button></a>
    </div>
    <div class="clear"></div>
</div>
<?php } else{
    ?>
    <?php if ( is_user_logged_in() ) { 
?>    

<div class="wrap pm_cumt">
        <?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();
            }  ?>
   
    <div class="after-range-area">
        <?php get_sidebar('exc'); 
            $terms = apply_filters( 'taxonomy-images-get-terms', '', array(
                'taxonomy' => 'excollection',
                'term_args' => array(
                'slug' => $term->slug,
                    )
                ) );
        ?>
            <div class="category_filter" id="cat-dropdown">
            
            <div class="cat-fillter">
                <h2>Filter by Category</h2>
                <form action="" method="get">
                    <?php
                    $taxonomies = array('category');
                    $args = array('orderby'=>'name','cat'=>-13,'hide_empty'=>false);
                    $select = get_terms_dropdown_typesssss($taxonomies, $args, $cat1);
                    $select = preg_replace("#<select([^>]*)>#", "<select$1 id='pro_cat1' >", $select);
                    echo $select;
                    ?>
                </form>
            </div>


        <?php 
            if($cat1 != ''){
                $args1 = array( 
                        'post_type' => 'products', 
                        'posts_per_page' =>-1,
                        'order'=>'ASC',
                        'category_name'=> $cat1, 
                        'tax_query' =>  array (
                            array(
                                'taxonomy'  => 'excollection',
                                'field'     => 'slug',
                                'terms'     => $term->slug,
                                
                            )
                            
                        )
                    );
                    $wpex_query = new WP_Query( $args1 );
                    while ( $wpex_query->have_posts() ) : $wpex_query->the_post();?>
                    <div class="ex-exclusive_box" id="ex-box-grid">
                                
                        <ul>
                            <li>
                               <a href="<?php the_permalink(); ?>" title=""><?php echo get_the_post_thumbnail( $page->ID, 'featured-image' ); ?> </a>
                               <h3><?php the_title(); ?></h3>
                            </li>
                        
                        </ul>
                    </div>
            <?php endwhile; }else { ?>

                <div id="exclusive-blog" class="tax_content">
                     <div class="image_tax">
                        <?php
                         echo wp_get_attachment_image( $terms[0]->image_id, '' );
                         ?>
                     </div> 
               </div>
            </div>


	   </div>	

            <div class="ex-boxs">
               	<div class="tax_product"> <?php  echo $title . " " .Products;  ?> </div>
              
             <?php if ( have_posts() ) :
					 while ( have_posts() ) : the_post(); 
					   $category = get_category_by_slug( 'exclusive-collection' );?>
                            <div class="ex-exclusive_box">
                                <ul>
                                    <li>
                                        <a href="<?php the_permalink(); ?>" title=""><?php echo get_the_post_thumbnail( $page->ID, 'featured-image' ); ?> </a>
                                       <h3><?php the_title(); ?></h3>
                                    </li>
                                </ul>
                            </div>
                        <?php endwhile; ?>
                        <?php echo wp_pagenavi(); ?>

                    <?php endif; }?>
                    <div class="clear"></div>
             </div>
             <div class="clear"></div>
            
             </div>
   <?php  } }

    ?>
    </section>

    <script type="text/javascript">
        jQuery(document).on("change","#pro_cat1",function(){
    var cat1 = jQuery(this).val();
    
    var path1 = "<?php echo $path_product1;?>?cat1234="+cat1;
    window.location=path1;
});
    </script>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
