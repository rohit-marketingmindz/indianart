<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<div class="footer_one" id="footer-area">
    <div class="footer-wrap">
      <div class="footer-one">
        <ul id="footer-content">
          <li>
          	<h2>Product Range</h2>
            <?php 
$max = 4; //number of categories to display
$taxonomy = 'excollection';
$terms = get_terms($taxonomy, 'orderby=name&order= ASC&hide_empty=0');

// Random order
shuffle($terms);

// Get first $max items
$terms = array_slice($terms, 0, $max);

// Sort by name
usort($terms, function($a, $b){
  return strcasecmp($a->name, $b->name);
});

// Echo random terms sorted alphabetically
 ?><ul id="footer-content"><?php
if ($terms) {
  foreach($terms as $term) {
    echo '<li><p><a href="' .get_term_link( $term, $taxonomy ) . '" title="' .  sprintf( __( "View all posts in %s" ), $term->name ) . '" ' . '>' . $term->name.'</a></p></li>';
  }
}
?> </ul>



    				 
           		
         	</li>
        </ul>
      </div>
      <div class="footer-two">
        <ul id="footer-content" class="social-icon">
          <h2>Follow Us</h2>
          <li> 
              <a href="<?php echo get_option('wpc_fblink'); ?>" target="_blank"><img src="<?php echo get_option('wpc_fbimg'); ?>"/></a> 
              <a href="<?php echo get_option('wpc_twtlink'); ?>" target="_blank"><img src="<?php echo get_option('wpc_twtimg'); ?>"/></a> 
              <a href="<?php echo get_option('wpc_lnkdinlink'); ?>" target="_blank"><img src="<?php echo get_option('wpc_lnkdinimg'); ?>"/></a> 
              <a href="<?php echo get_option('wpc_pintrstlink'); ?>" target="_blank"><img src="<?php echo get_option('wpc_pintrstimg'); ?>"/></a> 
              <a href="<?php echo get_option('wpc_gpluslink'); ?>" target="_blank"><img src="<?php echo get_option('wpc_gplusimg'); ?>"/></a> 
              <a href="<?php echo get_option('wpc_instagrmlink'); ?>" target="_blank"><img src="<?php echo get_option('wpc_instagrmimg'); ?>"/></a> 
          </li>
        </ul>
      </div>  
      <div class="footer-three">
        <ul id="footer-content">
          <h2>Quick Links</h2>
          <li>
            <?php $defaults = array( 'theme_location' => '', 'menu'=> 'footer_menu','container'=>'header_menus'); wp_nav_menu( $defaults ); ?>
            <li class="last-menu-footer"><a href="<?php echo get_page_link( get_page_by_title('Events')) ?>"><?php _e('Fairs'); ?></a></li>
          </li>
        </ul>
      </div>
      <div class="footer-four">
        <ul id="footer-content">
          <li>
            <?php dynamic_sidebar('contactus'); ?>
          </li>
        </ul>
      </div>
      <div class="clear"></div>
    </div>
      <hr />
    <div class="footer_bottom">
      <div id="news-latter">
        <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
      
        <style type="text/css">
          #mc_embed_signup
          {
            background:#fff;
            clear:left;
            font:14px Helvetica,Arial,sans-serif;
            width:100%;
          }
        </style>
    
        <div id="mc_embed_signup">
          <form action="//indianartfurnitures.us13.list-manage.com/subscribe/post?u=c1960535de734a3ce56e7a807&amp;id=185c2a6ecf" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
              <label for="mce-EMAIL">Newsletter</label>
              <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
               <div  style="position: absolute; left: -5000px;" aria-hidden="true" >
                  <input type="text" name="b_c1960535de734a3ce56e7a807_185c2a6ecf" tabindex="-1" value="">
               </div>
                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
              </div>  
            </form>
        </div>
      </div>

      <div id="copyright-info">
        <h6>
          <?php echo get_option('wpc_copytext'); ?>
        </h6>
      </div>
    </div>
</div>
</div>
	<?php wp_footer(); ?>
</body>

<script type="text/javascript">
  jQuery(function(){
    var jw = jQuery(window).width();
    //alert(jw);
    if(jw<1025){
      jQuery("#menu-header_menu li ul.sub-menu").parent("li").find("a").after("<span class='arrow_up'>&nbsp;</span>")
      jQuery("#menu-header_menu li ul li span").remove();
      jQuery(".mm-header_menus .mm-mobile_menu").click(function(){
        jQuery(".mm-header_menus ul li ul.sub-menu").hide();
      });
      jQuery("#menu-header_menu li ").on("click", "span.arrow_up", function(){
        jQuery(this).next("ul.sub-menu").slideToggle();
      });
      jQuery("#menu-header_menu li ").on("click", "span.arrow_up", function(){
        if(jQuery(this).hasClass('arrow_down')){
          jQuery(this).removeClass('arrow_down');
        }else{
          jQuery(this).addClass('arrow_down');
        }
      });
    }
  });
</script>

</html>