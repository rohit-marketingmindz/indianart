<?php
/**
 * The sidebar containing the secondary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
  ?>
<div class="exclusive-sidebar">
  <ul class="exclusive-mm">
    <li>
    	<?php
			$terms = apply_filters( 'taxonomy-images-get-terms', '', array(
			'taxonomy' => 'excollection',
			'hide_empty' => false,
			'term_args' => array(
			'slug' => $term->slug,
			)
			) );
			?>
            <ul id="mm-ex-sidebar">
              <li>
                <h2>Product Range</h2>
              </li>
                <?php foreach ((array) $terms as $term) { 
                
                ?>
              <li>
                <a href="<?php echo esc_attr(get_term_link($term, $taxonomy)); ?>" title=""><?php echo $term->name; ?></a>
              </li>
                <?php } ?>
            </ul>   
        <?php /*?><?php dynamic_sidebar('about'); ?>
        <a class="first-col" href="<?php echo home_url('/about-us/'); ?>">Read More</a> <?php */?>
                <hr>
   	</li> 
  </ul>
</div>