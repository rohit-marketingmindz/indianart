<?php
ob_start();
/**
 * Template name:Product Page Template11
 * 
 */
session_start();


global $post;
$post_int_id = "";
get_header(); 

$path_product = $_SERVER['REQUEST_URI'];
$path_arr = explode("?",$path_product);
$path_product = $path_arr[0];
$cat = "";
$myadd_tocart ='';
if(isset($_REQUEST['cat123']))
  {
	  $cat = trim($_REQUEST['cat123']);
  }
$path_product1 = $_SERVER['REQUEST_URI'];
$path_arr1 = explode("?",$path_product);
$path_product1 = $path_arr1[0];
$cat1 = "";
if(isset($_REQUEST['cat1234']))
  {
	  $cat1 = trim($_REQUEST['cat1234']);
  }
?>
<section class="content_block_background" id="cbb">
    <div id="page-heading">
        <h2 class="page-title"><?php the_title(); ?></h2>
        <?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
    </div>    

    <?php $redirect_url = home_url('/exclusive-collection/'); ?>
     
<!-- Custom Login Form -->
    <div id="custom_login">
        <?php if ( !is_user_logged_in()) { ?>    
            <span class="custom-login-form-heading"> Login For Exclusive Collection </span>
            <div id="custom-login-form" class="mm-coustom-form">
                <?php
                $args = array(
                    'echo'           => true,
                    'remember'       => false,
                    'redirect'       =>  $redirect_url,
                    'form_id'        => 'product-loginform',
                    'label_username' => __( '' ),
                    'label_password' => __( '' ),
                    'label_log_in'   => __( 'Log In' ),
                    'id_username'    => 'pr_user_login',
                    'id_password'    => 'pr_user_pass',
                    'id_submit'      => 'pr_wp_submit',
                    'value_username' => ''
                ); 
                wp_login_form( $args );
                ?>
                <input type="button" class="sign-up-button" onclick="location.href='<?php echo site_url() . '/sign-up/' ?>'" value="Sign Up" />

                <span class="custom-login-error_message">
                    <?php
                        $p_login = $_REQUEST['p_login'];
                        if($p_login == 'failed')
                        {
                            echo 'Invalid username or password';
                        }
                    ?>
                </span>
            </div>    
        <?php } ?> 
    </div>
<!-- Custom Login Form -->  

    <div class="wrap pm_cumt">
	    <div class="list_mm">
            <div class="range_filter">
		      <h2>Filter by Range</h2>
    		    <form action="" method="get">
    			    <?php
    			    $taxonomies = array('range');
    			    $args = array('orderby'=>'name','hide_empty'=>false);
    			    $select = get_terms_dropdown_type($taxonomies, $args, $cat);

    			    $select = preg_replace("#<select([^>]*)>#", "<select$1 id='pro_cat' >", $select);
    			    echo $select;
    			    ?>
    		    </form>
	        </div>
    
	       <div class="category_filter">
	           <h2>Filter by Category</h2>
        	    <form action="" method="get">
            	    <?php
            	    $taxonomies = array('category');
            	    $args = array('orderby'=>'name','cat'=>-13,'hide_empty'=>false);
            	    $select = get_terms_dropdown_typesssss($taxonomies, $args, $cat1);
            	    $select = preg_replace("#<select([^>]*)>#", "<select$1 id='pro_cat1' >", $select);
            	    echo $select;?>
        	    </form>
            </div>
        </div>
     	<?php $myadd_tocart = $_SESSION['arr_exp']; ?>
    	<div class="boxs">
            <?php 
                $events = $wpdb->get_results('SELECT name FROM  ' .$wpdb->prefix. 'terms');
                       $pro = array();
                       foreach($events as $even){
                           $pro[] = $even->name;
                       }
    				$args = array( 
                            'post_type' => 'products', 
                            'posts_per_page' =>-1,
                            'order'=>'ASC',
                            'range'=>$cat,
                            'category_name'=> $cat1, 
                            'tax_query' =>  array (
                                array(
                                    'taxonomy'  => 'excollection',
                                    'field'     => 'slug',
                                    'terms'     => $pro,
                                    'operator'  => 'NOT IN'
                                )
                            )
                        );
    				$wpex_query = new WP_Query( $args );
    				while ( $wpex_query->have_posts() ) : $wpex_query->the_post();
    				//'paged' => $paged,
    				?>
                    <?php $post_int_id = get_the_ID();
                        if(in_array($post_int_id, $myadd_tocart)){

                            $style = 'style="pointer-events:none;    background: rgba(0, 0, 0, 0.49);"';
                            $inquire = 1;
                            //$src = get_template_directory_uri() . '/images/add-to-cart-light.png';
                        } else {
                            $style = '';
                            $inquire = 0;
                            //$src = get_template_directory_uri() . '/images/add-to-cart-dark.png';
                        }
                    ?>
                <div class="exclusive_box" data-product-id="<?php echo $post_int_id; ?>">
                    <div class="exclusive_img">
                    	<a  href="<?php the_permalink(); ?>"><?php
                            if ( has_post_thumbnail()):
                                the_post_thumbnail('featured-image');
                            endif;
                            ?>
                        </a>               
                    </div>
                    <div class="exclusive_text">
                        <h3>
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                    </div>
                    <div class="exclusive_inq">
                        <div id="mm_but_<?php echo $post_int_id;?>" class="but_mm mm_but" style="display:none;">
                            <a class="cc_<?php echo $post_int_id;?>" <?php echo $style; ?> onclick="add_product_contact('<?php echo $post_int_id;?>')" class="exp_int"><?php if($inquire == 1){_e('Enquired');}else{ _e('Enquire');} ?>
                            </a>
                        </div>
                    </div>  
                    <!-- Inqiry Popup strats here -->
                        <ul>
                            <div id="blanket" style="display:none;"></div>
                        	<div id="popUpDiv" style="display:none;">
                            	<!-- <a onclick="popup('popUpDiv')" class="closeenq" title="close"> 
                                    <?php //_e('X',''); ?>
                                </a> -->
                                <div class="content-express">
                                	<h2>
                                        <?php _e('Yours selected product has been added to Enquiry list.'); ?>
                                    </h2>
                                    <p style="color:#666666; font:12px/19px 'Century Gothic';">
                                        <?php _e('Please visit <strong>Contact Us</strong> page to enter details.',''); ?> 
                                    </p>
                                    <div id="ajax_msg"></div>
                                   <!--  <div class="two_buttons">
                                        <a onclick="popup('popUpDiv')">
                                            <div class="enq-buttons_cont">
                                                <?php //_e('Ok',''); ?>
                                            </div>
                                        </a>
                                        <div class="clear"></div>
                                    </div> -->
                                </div>
                            </div>
                        </ul>
                    <!-- Inqiry Popup end here -->
                </div>
            <?php endwhile;wp_reset_postdata(); ?>     
            <div class="clear"></div>
    	</div>	
    </div>
</section>

<script src="<?php bloginfo('template_url'); ?>/js/csspopup.js"></script>
<script>
    jQuery(document).ready(function(){
        jQuery("button#tog").click(function(){
            jQuery("#postbox").show("slow");
        });
    });
</script>
<script>
    jQuery(document).on("change","#pro_cat",function(){
    	var cat = jQuery(this).val();
    	var path = "<?php echo $path_product;?>?cat123="+cat;
    	window.location=path;
    });


    jQuery(document).on("change","#pro_cat1",function(){
    	var cat1 = jQuery(this).val();
    	var path1 = "<?php echo $path_product1;?>?cat1234="+cat1;
    	window.location=path1;
    });
</script>
<script>	 
	   function delete_product(prouctnuid){
		   //console.log(prouctid);

			jQuery.ajax({
					url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
					type: "POST",
  					data: { prouctnuid : prouctnuid,act:1},
					}).done(function(result) {
						proidss = result.split("--"); 
						jQuery("#popUpDiv").show();
						jQuery(".exp_enquiry").empty();
					    jQuery(".exp_enquiry").append(proidss[0]);
						jQuery('#ajax_msg').text();
						jQuery('#ajax_msg').text(proidss[1]);

					});
	  }
	 
	 function add_product_contact(prouctid)
     {
            var cc = '.cc_'+prouctid;
           //alert(cc);
            jQuery(cc).css('pointer-events', 'none');
            jQuery(cc).attr('src','<?php echo get_template_directory_uri()?>/images/add-to-cart-light.png');
            jQuery.ajax({
                url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
                type: "POST",
                data: { prouctid : prouctid },
                }).done(function(result) {
					proidss = result.split("--"); 
					console.log(result);
                    jQuery("#popUpDiv").show();
					jQuery(".exp_enquiry").empty();
					jQuery(".exp_enquiry").append(proidss[0]);
					jQuery('.proidsfromajax').val(proidss[2]);
					jQuery('#ajax_msg').text();
					jQuery('#ajax_msg').text(proidss[1]);
                    jQuery('#show_value').text(proidss[3]);
                     //jQuery(cc).text('Inquired');
                    //location.reload();
					//alert(proidss[3]);
                });
    }

    jQuery(document).ready(function() {
       
        jQuery( '.exclusive_box' ).hover(
          function() {
            var product_id = jQuery(this).data('product-id');
            var mm_but = '#mm_but_'+ product_id;   
            jQuery(mm_but).show();
          }, function() {
            var product_id = jQuery(this).data('product-id');
            var mm_but = '#mm_but_'+ product_id; 
            jQuery(mm_but).hide();
          }
        );
    });  
    
</script>
<?php get_footer(); ?>