<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
    <section class="content_block_background" id="cbb">
        <?php if( is_tax() ) {
    global $wp_query;
    $term = $wp_query->get_queried_object(); ?>
        <h2 class="page-title"><?php echo $title = $term->name; }?></h2>
            <div class="wrap">
                <div class="boxs">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
                        	<div class="box">
                                <ul>
                                    <li>
                                       <?php echo get_the_post_thumbnail( $page->ID, 'medium' ); ?>
                                       <h3><?php the_title(); ?></h3>
                                    </li>
                                    <div class="but_mm"><a href="<?php the_permalink(); ?>" title=""><?php _e('View More'); ?></a></div>
                                    <?php /*?><?php 
                                    echo '<li>' . '<a href="' . esc_attr(get_term_link($term, $taxonomy)) . '" 
                                    title="' . sprintf( __( "View all posts in %s" ), $term->name ) . '" ' . '>' . $term->name.'</a>'.'</li>'
                                    
                                    ?><?php */?>
                                </ul>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <div class="clear"> </div>
                </div>
            </div>
    </section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
